import sys


fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1: str, format2: str) -> bool:
    for i, f in enumerate(fordered):
        if f == format1:
            compresion1 = i
        if f == format2:
            compresion2 = i
    return compresion1 > compresion2

def find_lower_pos(formats: list, pivot: int) -> int:
    for i in range (pivot + 1, len(formats)):
        if lower_than(formats[i], formats[pivot]):
            return i
    return -1
    
def sort_formats(formats: list) -> list:
    sorted_formats = formats[:]
    for i in range(len(sorted_formats)):
        min_idx = i
        for j in range(i + 1, len(sorted_formats)):
            if lower_than(sorted_formats[j], sorted_formats[min_idx]):
                min_idx = j
        sorted_formats[i], sorted_formats[min_idx] = sorted_formats[min_idx], sorted_formats[i]
    return sorted_formats

def main():
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()

if __name__ == '__main__':
    main()

